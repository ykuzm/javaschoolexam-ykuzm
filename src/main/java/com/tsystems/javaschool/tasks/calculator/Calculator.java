package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (isCorrect(statement)) {
            // создаем массив символов из строки и добавляем скобки по краям
            char[] chars = ("(" + statement + ")").toCharArray();

            // создаем пустые списки для чисел и символов
            List<Double> numbers = new ArrayList<>();
            List<Character> signs = new ArrayList<>();
            StringBuilder sb = new StringBuilder();

            // создаем regexp с допустимыми символами (кроме скобок)
            String regexp1 = "[-+/*\\()]";

            // заполняем списки вещественными числами и символами математических операций и скобоок
            for (int i = 0; i < chars.length; i++) {
                if (Pattern.matches(regexp1, String.valueOf(chars[i]))) {
                    signs.add(chars[i]);
                    if ((i > 0) && (Pattern.matches(regexp1, String.valueOf(chars[i - 1])))) {
                        numbers.add(null);
                    } else if (i > 0) {
                        try {
                            int a = Integer.parseInt(sb.toString());
                            numbers.add(1.0 * a);
                        } catch (Exception e) {
                            double d = Double.parseDouble(sb.toString());
                            numbers.add(d);
                        }
                        sb = new StringBuilder();
                    }
                } else {
                    sb.append(chars[i]);
                }
            }

            // добавляем в список чисел последнее число, проверяя, есть ли оно
            if (!sb.toString().equals("")) {
                try {
                    int a = Integer.parseInt(sb.toString());
                    numbers.add(1.0 * a);
                } catch (Exception e) {
                    double d = Double.parseDouble(sb.toString());
                    numbers.add(d);
                }
            } else {
                numbers.add(null);
            }

            // задаем начальное положение левой скобки равной 0, чтобы хотя бы раз зайти в цикл
            int leftbracket = 0;

            do {
                // вычисляем положенние самой внутренней пары скобок
                leftbracket = findLeftBracket(signs);
                int rightbracket = findRightBracket(leftbracket, signs);

                // проводим вычисления выражения внутри скобок, если есть деление на null, то возвращаем null
                try {
                    if (leftbracket != 0) {
                        makeMath(numbers.subList(leftbracket, rightbracket), signs.subList(leftbracket + 1, rightbracket));
                    } else {
                        makeMath(numbers, signs.subList(1, signs.size()));
                    }
                }
                catch (ArithmeticException e) {
                    return null;
                }

                // удаляем лишние элементы из коллекций чисел и знаков
                if (leftbracket != 0) {
                    signs.remove(leftbracket);
                    signs.remove(leftbracket);
                    numbers.remove(leftbracket - 1);
                    numbers.remove(leftbracket);
                }
                else {
                    signs.clear();
                    numbers.remove(1);
                }
            } while (leftbracket != 0);

            // обрезаем у результата два последних символа, если результат целое число
            String res = String.valueOf(numbers.get(0));
            if ((res.charAt(res.length()-2) == '.') && (res.charAt(res.length()-1) == '0')) {
                res = res.substring(0, res.length()-2);
            }
            return res;
        }
        return null;
    }

    /**
     * Метод ищет положение открывающей скобки
     * @param signs последовательность символов
     * @return порядковый номер самой правой из открывающих скобок в последовательности
     */
    public static int findLeftBracket(List<Character> signs) {
        for (int i = signs.size()-1; i > -1; i--) {
            if (signs.get(i) == '(') {
                return i;
            }
        }
        return 0;
    }

    /**
     * Метод ищет положение закрывающей скобки
     * @param leftbracket положение самой правой из левых скобок в последовательности
     * @param signs последовательность символов
     * @return порядковый номер соответствующей закрывающей скобки в последовательности
     */
    public static int findRightBracket(int leftbracket, List<Character> signs) {
        if (leftbracket != -1) {
            for (int i = leftbracket; i < signs.size(); i++) {
                if (signs.get(i) == ')') {
                    return i;
                }
            }
        }
        return signs.size()-1;
    }

    /**
     * Метод проводит вычисления для заданных последовательностей
     * @param numbers последовательность чисел
     * @param signs последовательность символов
     */
    public static void makeMath(List<Double> numbers, List<Character> signs) {

        // проводим умножение и деление
        for (int i = 0; i < signs.size(); i++) {
            if (signs.get(i) == '*') {
                double res = numbers.get(i) * numbers.get(i+1);
                numbers.set(i, res);
                numbers.remove(i+1);
                signs.remove(i);
                i--;
            }
            else if (signs.get(i) == '/') {
                if (numbers.get(i+1) == 0) {
                    throw new ArithmeticException("Dividing by zero, man!");
                }
                else {
                    double res = numbers.get(i) / numbers.get(i + 1);
                    numbers.set(i, res);
                    numbers.remove(i + 1);
                    signs.remove(i);
                    i--;
                }
            }
        }

        // проводим сложение и вычитание
        for (int i = 0; i < signs.size(); i++) {
            if (signs.get(i) == '+') {
                double res = numbers.get(i) + numbers.get(i+1);
                numbers.set(i, res);
                numbers.remove(i+1);
                signs.remove(i);
                i--;
            }
            else if (signs.get(i) == '-') {
                double res = numbers.get(i) - numbers.get(i+1);
                numbers.set(i, res);
                numbers.remove(i+1);
                signs.remove(i);
                i--;
            }
        }
    }

    /**
     * Метод проверяет корректность входного выражения
     * @param s строковое представление математического выражения
     * @return true, если выражение удовлетворяет всем правилам, false в противном случае
     */
    public boolean isCorrect(String s) {

        // проверка на null и пустую строку
        if ((s == null) || (s.equals(""))) {
            return false;
        }

        // проверка на недопустимые символы в выражении + недопустимые цифры в начале строки + недопустимые цифры в
        // конце строки + наличие двух и более точек подряд + наличие нескольких точек в одном числе + наличие двух
        // знаков операции подряд + наличие знака перед закрывающей скобкой + наличие знака после закрывающей скобки +
        // наличие цифры перед открывающей скобкой + наличие цифры после закрывающей скобки
        String regex1 = ".+[^\\+\\-\\*\\/\\.\\(\\)0-9].+|^[\\-\\+\\*\\/\\)\\.].+|.+[\\-\\+\\*\\/\\(\\.]$|" +
                ".+[\\.][\\.].+|.+[\\.][0-9]+[\\.].+|.+[\\+\\-\\*\\/][\\+\\-\\*\\/].+|.+[\\+\\-\\*\\/][\\)].+|" +
                ".+[\\(][\\+\\-\\*\\/].+|.+[0-9][\\(].+|.+[\\)][0-9].+";
        if (Pattern.matches(regex1, s)) {
            return false;
        }

        // проверка на правильность расстановки скобок в выражении с помощью реализации стека на списке
        List<Character> list = new ArrayList();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                list.add('(');
            }
            else if (s.charAt(i) == ')') {
                if (list.size() != 0) {
                    list.remove(list.size()-1);
                }
                else {
                    return false;
                }
            }
        }
        if (list.size() != 0) {
            return false;
        }
        return true;
    }
}
