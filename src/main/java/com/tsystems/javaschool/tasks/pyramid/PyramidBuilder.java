package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

import static java.lang.Math.ceil;
import static java.lang.Math.sqrt;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        int[][] mass;

        // вычисляем количество строк в пирамиде
        int rownumber = (int) ceil((-1.0 + sqrt(1 + 8 * inputNumbers.size())) / 2);

        // проверяем, достаточно ли элементов для построения полной пирамиды
        if ((!inputNumbers.contains(null)) && (rownumber*(rownumber+1)/2 == inputNumbers.size())) {

            // сортируем коллекцию
            Collections.sort(inputNumbers);

            // заполняем нулями
            mass = new int[rownumber][2 * rownumber - 1];
            for (int row = 0; row < mass.length; row++) {
                for (int col = 0; col < mass[row].length; col++) {
                    mass[row][col] = 0;
                }
            }

            // вводим количество элементов, которые уже добавлены в пирамиду
            int number = 0;

            // определяем индекс первого элемента
            int startindex = mass[0].length / 2 + 1;

            // заполняем пирамиду
            for (int row = 0; row < mass.length; row++) {
                startindex = startindex - 1;
                for (int j = 0; j <= row; j++) {
                    if (number >= inputNumbers.size()) {
                        break;
                    }
                    mass[row][startindex + 2 * j] = inputNumbers.get(number);
                    number++;
                }
            }
        }
        else {
            throw new CannotBuildPyramidException();
        }
        return mass;
    }
}
