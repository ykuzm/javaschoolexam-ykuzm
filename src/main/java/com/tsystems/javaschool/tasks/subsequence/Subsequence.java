package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public static boolean find(List x, List y) throws IllegalArgumentException {
        int target = -1;
        if ((x != null) && (y != null)) {
            for (int required = 0; required < x.size(); required++) {
                if (target != -5) {
                    target = findElement(required, x, target + 1, y);
                }
                else {
                    break;
                }
            }
        }
        else {
            throw new IllegalArgumentException("At least one of the sequences is NULL!");
        }
        return (target != -5);
    }

    /**
     * Метод ищет элемент последовательности <code>x</code> в последовательности <code>y</code>
     * @param required номер искомого элемента в последовательности <code>x</code>
     * @param x последовательность, элемент которой ищется
     * @param target номер элемента последовательности <code>y</code>, начиная с которого производится
     *               поиск
     * @param y последовательность, в которой ищется элемент
     * @return номер искомого элемента, на котором он находится в последовательности <code>y</code>. Если
     * такого элемента нет, то возвращает -5
     */
    public static int findElement(int required, List x, int target, List y) {
        for (int i = target; i < y.size(); i++) {
            if (x.get(required).equals(y.get(i))) {
                return i;
            }
        }
        return -5;
    }
}
